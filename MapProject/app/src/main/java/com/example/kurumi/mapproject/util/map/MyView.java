package com.example.kurumi.mapproject.util.map;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.kurumi.mapproject.R;

/**
 * Created by kurumi on 2015/06/22.
 */
public class MyView extends LinearLayout{
    public MyView(Context context) {
        super(context);

    View layout = LayoutInflater.from(context).inflate(R.layout.map_alert, this);
    final TextView textView = (TextView)layout.findViewById(R.id.textView);
    }


    public MyView(Context context, AttributeSet attrs) {
        super(context, attrs);
      //  init(attrs, 0);
        View layout = LayoutInflater.from(context).inflate(R.layout.map_alert, this);
        final TextView textView = (TextView)layout.findViewById(R.id.textView);
    }

    public MyView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        View layout = LayoutInflater.from(context).inflate(R.layout.map_alert, this);
        final TextView textView = (TextView)layout.findViewById(R.id.textView);
    //    init(attrs, defStyle);
    }

}
