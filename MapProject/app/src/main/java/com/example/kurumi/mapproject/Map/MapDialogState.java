package com.example.kurumi.mapproject.Map;

import java.io.Serializable;

/**
 * Created by kurumi on 2015/09/06.
 */
public class MapDialogState implements Serializable {
    private int period;
    private boolean check1;
    private boolean check2;
    private boolean check3;
    private boolean check4;

    public MapDialogState(int period, boolean check1, boolean check2, boolean check3, boolean check4) {
        this.period = period;
        this.check1 = check1;
        this.check2 = check2;
        this.check3 = check3;
        this.check4 = check4;
    }

    public int getPeriod() {
        return period;
    }

    public boolean isCheck1() {
        return check1;
    }

    public boolean isCheck2() {
        return check2;
    }

    public boolean isCheck3() {
        return check3;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public void setCheck1(boolean check1) {
        this.check1 = check1;
    }

    public void setCheck2(boolean check2) {
        this.check2 = check2;
    }

    public void setCheck3(boolean check3) {
        this.check3 = check3;
    }

    public boolean isCheck4() {
        return check4;
    }

    public void setCheck4(boolean check4) {
        this.check4 = check4;
    }
}
