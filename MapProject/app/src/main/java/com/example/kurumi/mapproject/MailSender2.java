package com.example.kurumi.mapproject;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.kurumi.mapproject.database.ContentDAO;
import com.example.kurumi.mapproject.database.ContentData;
import com.example.kurumi.mapproject.database.MySQLiteOpenHelper;
import com.sun.mail.imap.IMAPFolder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.mail.FetchProfile;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.SearchTerm;

/**
 * Created by kurumi on 2015/07/12.
 */
public class MailSender2 {

    String host = "imap.gmail.com";
    int port = 993;
    String user  = "sample.javamail@gmail.com";
    String password = "samplemail";
    String target_folder = "INBOX";
    private Context context;
    private SQLiteDatabase db;
    private ArrayList<ContentData> mailContent=new ArrayList<>();
    String encode[]=new String[]{"痴漢発生","わいせつ","不審者","子供安全"};


    public MailSender2(Context context){
        this.context=context;
    }


    public void getMailContent(){

        Properties props = System.getProperties();
        Session sess = Session.getInstance(props, null);
        //範囲設定

        SearchTerm searchCondition = new SearchTerm() {
            @Override
            public boolean match(Message message) {
                try {
                    if (message.getSubject().contains("子供安全情報")||message.getSubject().contains("痴漢")) {
                        return true;
                    }
                } catch (MessagingException ex) {
                    ex.printStackTrace();
                }
                return false;
            }
        };






        Store st = null;
        try {
            st = sess.getStore("imaps");
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        try {
            assert st != null;
            st.connect(host, port, user, password);

            Folder fol = st.getFolder(target_folder);

            if(fol.exists()){
                fol.open(Folder.READ_ONLY);

//                Message[] foundMessages =fol.search(searchCondition);
                Message[] foundMessages =fol.getMessages();
System.out.println(foundMessages.length);

                FetchProfile fp = new FetchProfile();
                fp.add(FetchProfile.Item.ENVELOPE);
                fp.add(IMAPFolder.FetchProfileItem.FLAGS);
                fp.add(IMAPFolder.FetchProfileItem.CONTENT_INFO);
                fp.add("X-mailer");
                fol.fetch(foundMessages, fp);


                for(Message m : foundMessages){
                    System.out.println("yaaa");
                    //メッセージループ
                    if(m.isMimeType("text/plain")) {

                        //タイトル検索がバグっているので、ここで対応？？
                        if (m.getSubject().contains("わいせつ") || m.getSubject().contains("不審者")||m.getSubject().contains("子供安全情報")||m.getSubject().contains("痴漢")) {


                            String[] split_content = m.getContent().toString().split("[◆◎]");
                            //必要な情報だけに分解する
                            if (split_content.length >= 4) {
                                mailContent.add(new ContentData(encodeCategory(m.getSubject()),
                                        time_format(split_content[1].replaceAll("\n", ""), m.getReceivedDate()),
                                        split_content[2].replaceAll("\n", ""), split_content[3].replaceAll("\n", ""), split_content[4].replaceAll("\n", "")));
                            }
                            //必要な情報だけに分解する
                            //         mailContent.add(new ContentData(split_content[1],split_content[2],split_content[3]));
                        }
                    }
                }
                fol.close(false);
System.out.println("VAIO");
                //dbに保存
                MySQLiteOpenHelper dbHelper = new MySQLiteOpenHelper(context);
                db = dbHelper.getWritableDatabase();
                ContentDAO contentDAO = new ContentDAO(db, dbHelper);
                contentDAO.insert(mailContent);
            }
            st.close();
        } catch (MessagingException | IOException e) {
            e.printStackTrace();
        }
    }

    private int encodeCategory(String subject) {
        for(int i=0;i<=encode.length;i++){
            if(subject.matches(".*" + encode[i] + ".*")){
                return i;
            }
        }
        return 0;
    }
    //フォーマットを整える
    private String time_format(String s, Date receivedDate) {

        Calendar datetime = Calendar.getInstance();
        datetime.setTime(receivedDate);
        int index = s.indexOf("年");
        if(index!=-1){
            s="日時："+s.substring(index+1);
        }
        s = s.replace("本日", String.valueOf(datetime.get(Calendar.MONTH)+1)+"月"+String.valueOf(datetime.get(Calendar.DAY_OF_MONTH))+"日");
        datetime.add(Calendar.DAY_OF_MONTH,-1);
        s = s.replace("昨日", String.valueOf(datetime.get(Calendar.MONTH)+1)+"月"+String.valueOf(datetime.get(Calendar.DAY_OF_MONTH))+"日");

        StringBuilder sb = new StringBuilder(s);
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if ('０' <= c && c <= '９') {
                sb.setCharAt(i, (char) (c - '０' + '0'));
            }
        }
        sb.insert(3, String.valueOf(datetime.get(Calendar.YEAR)) + "年");

        //  String w=sb.toString();


        return sb.toString();
    }


}