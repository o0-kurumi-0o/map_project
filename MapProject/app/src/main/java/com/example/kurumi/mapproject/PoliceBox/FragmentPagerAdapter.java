package com.example.kurumi.mapproject.PoliceBox;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

public class FragmentPagerAdapter extends android.support.v4.app.FragmentPagerAdapter {

    private String tabTitles[] = new String[]{
            "北区","上京区", "左京区","中京区","東山区","下京区","南区", "右京区", "伏見区", "山科区","西京区","福知山",};

    private static final int PAGE_COUNT = 5;
    private final PoliceBoxActivityFragment fragment;

    public FragmentPagerAdapter(FragmentManager fm, PoliceBoxActivityFragment fragment) {
        super(fm);
        this.fragment = fragment;
    }

    @Override
    public Fragment getItem(int position) {

        return PoliceBoxActivityFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item category
        return tabTitles[position];
    }

    public void setText(String text) {
        this.tabTitles[0] = text;
    }
}