package com.example.kurumi.mapproject.News;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.kurumi.mapproject.R;
import com.example.kurumi.mapproject.database.ContentData;

import java.util.ArrayList;

/**
 * Created by kurumi on 2015/08/18.
 */
public class NewsArrayAdapter extends ArrayAdapter<ContentData>{

    public NewsArrayAdapter(Context context, int resource, ArrayList<ContentData> objects) {
        super(context, resource, objects);
     //   this.materialises=objects;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listview_inner, parent, false);
        }

        final ContentData items = getItem(position);
        // Lookup view for data population
        TextView tvTime = (TextView) convertView.findViewById(R.id.textView4);
        TextView tvPosition = (TextView) convertView.findViewById(R.id.textView5);
        TextView tvContent=(TextView)convertView.findViewById(R.id.textView6);
        TextView tvMen=(TextView)convertView.findViewById(R.id.textView7);
        // Populate the data into the template view using the data object
        /*
         */
        tvTime.setText(items.getTime());
        tvPosition.setText(items.getPlace());
        tvContent.setText(items.getContent());
        tvMen.setText(items.getMen());
        // Return the completed view to render on screen

        return convertView;
    }
}
