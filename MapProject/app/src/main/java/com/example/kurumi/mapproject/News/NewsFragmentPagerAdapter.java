package com.example.kurumi.mapproject.News;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 * Created by kurumi on 2015/09/11.
 */
public class NewsFragmentPagerAdapter extends android.support.v4.app.FragmentPagerAdapter {
    private String tabTitles[] = new String[]{
            "すべてのニュース","痴漢犯罪情報", "公然わいせつ情報","不審者情報","子供安全情報"};

    private static final int PAGE_COUNT = 5;
    private final NewsActivityFragment fragment;

    public NewsFragmentPagerAdapter(FragmentManager fm, NewsActivityFragment fragment) {
        super(fm);
        this.fragment = fragment;
    }

    @Override
    public Fragment getItem(int position) {

        return NewsActivityFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item category
        return tabTitles[position];
    }

    public void setText(String text) {
        this.tabTitles[0] = text;
    }
}