package com.example.kurumi.mapproject.Alert;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.kurumi.mapproject.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class AlertActivityFragment extends Fragment {

    private ImageButton alertButton;
    private MediaPlayer bgm;

    public AlertActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_alert, container, false);
        alertButton = (ImageButton) rootView.findViewById(R.id.alertButton);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bgm = MediaPlayer.create(getActivity(), R.raw.su684);
        alertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!bgm.isPlaying()){
                    try {
                        bgm.prepare();
                    } catch (Exception ignored) {
                    }
                    bgm.setLooping(true);
                    bgm.start();
                }else{
                    bgm.stop();
                }
            }
        });
    }

}
