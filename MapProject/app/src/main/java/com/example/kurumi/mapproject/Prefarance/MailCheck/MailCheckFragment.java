package com.example.kurumi.mapproject.Prefarance.MailCheck;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.kurumi.mapproject.Dialog.ErrorShowDialogFragment;
import com.example.kurumi.mapproject.Dialog.ProgressDialogFragment;
import com.example.kurumi.mapproject.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class MailCheckFragment extends Fragment implements LoaderManager.LoaderCallbacks<Boolean> {

    private Button button;
    private EditText editText1;
    private EditText editText2;
    private EditText editText3;
    private EditText editText4;
    private ProgressDialogFragment progressDialog;
    private SharedPreferences sharedPref;
    private TextView textView;

    public MailCheckFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_mail_check, container, false);
        button = (Button) rootView.findViewById(R.id.button5);
        editText1 = (EditText) rootView.findViewById(R.id.editText);
        editText2 = (EditText) rootView.findViewById(R.id.editText2);
        editText3 = (EditText) rootView.findViewById(R.id.editText3);
        editText4 = (EditText) rootView.findViewById(R.id.editText4);
        textView=(TextView)rootView.findViewById(R.id.textView10);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        editText1.setText(sharedPref.getString("user", "sample.javamail@gmail.com"));
        editText2.setText(sharedPref.getString("password", "samplemail"));
        editText3.setText(sharedPref.getString("port", "993"));
        editText4.setText(sharedPref.getString("host", "imap.gmail.com"));


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager connectivity = (ConnectivityManager) getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo network = connectivity.getActiveNetworkInfo();
                if (network == null) {
                    errorShow(getResources().getString(R.string.error_message_no_network));
                } else {
                    if (network.isConnected()) {
                        Bundle args = new Bundle();
                        args.putString("a", editText1.getText().toString());
                        args.putString("b", editText2.getText().toString());
                        args.putString("c", editText3.getText().toString());
                        args.putString("d", editText4.getText().toString());
                        getLoaderManager().restartLoader(2, args, MailCheckFragment.this);
                    } else {
                        errorShow(getResources().getString(R.string.error_message_no_network));
                    }
                }
            }
        });
    }

    private void errorShow(String context) {
        ErrorShowDialogFragment errorShowDialogFragment = new ErrorShowDialogFragment();
        Bundle message = new Bundle();
        message.putString("title", getResources().getString(R.string.error_title));
        message.putString("message", context);
        errorShowDialogFragment.setArguments(message);
        errorShowDialogFragment.show(getChildFragmentManager(), "error");
    }


    @Override
    public Loader<Boolean> onCreateLoader(int id, Bundle args) {
        String data1 = args.getString("a");
        String data2 = args.getString("b");
        String data3 = args.getString("c");
        String data4 = args.getString("d");
        progressDialog = new ProgressDialogFragment();
        Bundle message = new Bundle();
        message.putString("tag", "アドレス確認中");
        progressDialog.setArguments(message);
        progressDialog.show(getChildFragmentManager(), "error");
        return new MailCheckAsyncTaskLoader(getActivity(), data1, data2, data3, data4);
    }

    @Override
    public void onLoadFinished(Loader<Boolean> loader, Boolean data) {
        if (progressDialog != null) {
            progressDialog.onDismiss(progressDialog.getDialog());
        }
        if (data) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("user", editText1.getText().toString());
            editor.putString("password", editText2.getText().toString());
            editor.putString("port", editText3.getText().toString());
            editor.putString("host", editText4.getText().toString());
            editor.apply();
            textView.setVisibility(View.VISIBLE);
            button.setClickable(false);


        } else {
            errorShow(getResources().getString(R.string.error_message_no_mail_settings));
        }
    }

    @Override
    public void onLoaderReset(Loader<Boolean> loader) {

    }
}
