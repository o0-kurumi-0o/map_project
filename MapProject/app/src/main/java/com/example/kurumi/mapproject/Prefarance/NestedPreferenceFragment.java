package com.example.kurumi.mapproject.Prefarance;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.example.kurumi.mapproject.R;

/**
 * Created by kurumi on 2015/09/11.
 */
public class NestedPreferenceFragment extends PreferenceFragment {


    private static final String TAG_KEY = "NESTED_KEY";

    public static NestedPreferenceFragment newInstance(int key) {
        NestedPreferenceFragment fragment = new NestedPreferenceFragment();
        // supply arguments to bundle.
        Bundle args = new Bundle();
        args.putInt(TAG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_data_nested);

    }
}
