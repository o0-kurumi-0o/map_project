package com.example.kurumi.mapproject.AsyncTask;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.kurumi.mapproject.R;
import com.example.kurumi.mapproject.database.ContentData;
import com.example.kurumi.mapproject.database.PositionData;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by kurumi on 2015/07/12.
 */
class Async extends AsyncTask<Void, Void, Void> {

    private HashMap<Marker, PositionData> eventMarkerMap;
    private Context mContext;
    private GoogleMap map;
    private ArrayList<ContentData> arrayList = new ArrayList<>();
    private ArrayList<ContentData> sub = new ArrayList<>();
    private ArrayList<PositionData> positionDatas = new ArrayList<>();

    private Float[] markerColor={BitmapDescriptorFactory.HUE_YELLOW,BitmapDescriptorFactory.HUE_GREEN,BitmapDescriptorFactory.HUE_RED,BitmapDescriptorFactory.HUE_MAGENTA};


    public Async(Context mContext, GoogleMap mMap, ArrayList<ContentData> arrayList, HashMap<Marker, PositionData> eventMarkerMap) {
        this.mContext = mContext;
        this.map = mMap;
        this.arrayList = arrayList;
        this.eventMarkerMap=eventMarkerMap;
    }

    public Async(Context content, GoogleMap mMap, ArrayList<ContentData> arrayList) {
        this.mContext = content;
        this.map = mMap;
        this.arrayList = arrayList;

    }


    @Override
    protected Void doInBackground(Void... params) {
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        for (int i = 0; i < arrayList.size(); i++) {
            sub.add(new ContentData(
                    arrayList.get(i).getTime(), all_place_content(arrayList.get(i).getPlace()), arrayList.get(i).getContent(),
                    arrayList.get(i).getMen(), markerColor[arrayList.get(i).getCategory()]));
            try {
                PositionData w = changeFormat(geocoder, i);
                if(w!=null){
                    w.setIncident_or_policeBox(true);
                    positionDatas.add(w);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private PositionData changeFormat(Geocoder geocoder, int position) throws IOException {
        List<Address> addressList = geocoder.getFromLocationName(sub.get(position).getPlace(), 1);
        if(addressList.size()!=0){
        Address address = addressList.get(0);
        double lat = address.getLatitude();
        double lng = address.getLongitude();
        return new PositionData(lat, lng, sub.get(position).getPlace(),
                sub.get(position).getPlace(),sub.get(position).getTime(),sub.get(position).getContent(),sub.get(position).getMen(),sub.get(position).getColor());
    }
        return null;
    }

    private String all_place_content(String place) {
        //左分解
        int index_left = place.indexOf("：");
        if (index_left > 1) {
            place = place.substring(index_left + 1);
        }
        //右分解
        int index_right_1 = place.indexOf("の");

        if (index_right_1 > 1) {
            place = place.substring(0, index_right_1);
        }
        int index_right_2 = place.indexOf("付近");
        if (index_right_2 > 1) {
            place = place.substring(0, index_right_2);
        }
        return place;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        for (int i = 0; i < positionDatas.size(); i++) {
            //        markerList.add(new MarkerOptions().position(new LatLng(34.985442, 135.758466)).title("Welcome Kyoto "));
            //   map.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title(sub.get(i).getPlace()));
            Marker marker=map.addMarker(new MarkerOptions().position(new LatLng(positionDatas.get(i).getLat(),
                    positionDatas.get(i).getLng())).title(positionDatas.get(i).getTitle()).
                    icon(BitmapDescriptorFactory.defaultMarker(positionDatas.get(i).getColor())));

            CircleOptions circleOptions = new CircleOptions()
                    .center(new LatLng(marker.getPosition().latitude, marker.getPosition().longitude))
                    .radius(100).fillColor(mContext.getResources().getColor(R.color.map_circle)).strokeColor(mContext.getResources().getColor(R.color.map_circle));
            map.addCircle(circleOptions);



            eventMarkerMap.put(marker, positionDatas.get(i));
            /*

            CircleOptions circleOptions = new CircleOptions()
                    .center(new LatLng(positionDatas.get(i).getLat(), positionDatas.get(i).getLng()))
                    .radius(100).fillColor(mContext.getResources().getColor(R.color.map_circle)).strokeColor(mContext.getResources().getColor(R.color.map_circle));
            Circle circle = map.addCircle(circleOptions);
*/
        }

        Toast.makeText(mContext,"読み込み完了",Toast.LENGTH_SHORT).show();
    }

    public void setPositionDatas(ArrayList<ContentData> positionDatas) {
        this.arrayList = positionDatas;
    }


}