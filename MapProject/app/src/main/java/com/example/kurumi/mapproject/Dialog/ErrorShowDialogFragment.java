package com.example.kurumi.mapproject.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

/**
 * Created by kurumi on 2015/04/05.
 */
public class ErrorShowDialogFragment extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String title=getArguments().getString("title","");
        String message=getArguments().getString("message","");

        builder.setTitle(title).setMessage(message).setPositiveButton("OK",null);

        return builder.create();
    }
}
