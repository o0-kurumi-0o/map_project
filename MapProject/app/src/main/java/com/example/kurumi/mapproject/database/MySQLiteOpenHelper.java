package com.example.kurumi.mapproject.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.kurumi.mapproject.PoliceBox.content.policeBoxList;

/**
 * Created by kurumi on 2015/03/24.
 * SQLiteとのやりとりをまとめる
 */
public class MySQLiteOpenHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "main.db";

    private static final String TABLE_MAP = "table_map";
    // Common column names
    private static final String KEY_ID = "_id";

    //menu_main
    private static final String KEY_CATEGORY = "category";
    private static final String KEY_TIME = "time";
    private static final String KEY_PLACE = "place";
    private static final String KEY_CONTENT = "content";
    private static final String KEY_MEN = "men";

    //policeBox
    private static String TABLE_POLICE="table_policeBox";
    private static final String KEY_WARD = "ward";
    private static final String KEY_LAT = "lat";
    private static final String KEY_LON = "lon";
    private static final String KEY_NAME = "name";
    private static final String KEY_PHONE = "phone";


    private static final String CREATE_TABLE_MAP = "CREATE TABLE " + TABLE_MAP + "(" +
            KEY_ID + " INTEGER PRIMARY KEY," +
            KEY_CATEGORY + " TEXT," +
            KEY_TIME + " TEXT," +
            KEY_PLACE + " TEXT," +
            KEY_CONTENT + " TEXT," +
            KEY_MEN + " TEXT" +
            ")";

    private static final String CREATE_TABLE_POLICEBOX= "CREATE TABLE " + TABLE_POLICE + "(" +
            KEY_ID + " INTEGER PRIMARY KEY," +
            KEY_WARD + " INTEGER," +
            KEY_PLACE + " TEXT," +
            KEY_LAT + " REAL," +
            KEY_LON + " REAL," +
            KEY_NAME + " TEXT," +
            KEY_PHONE + " TEXT" +
            ")";


    public MySQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_MAP);
        db.execSQL(CREATE_TABLE_POLICEBOX);
       insert(db);
    }

    private void insert(SQLiteDatabase db) {
        for(int i=0;i< policeBoxList.policeContents.length;i++) {
            db.execSQL("insert into table_policeBox(ward,place,lat,lon,name,phone) values ("+policeBoxList.policeContents[i].getPoliceBoxTag()+"," +
                    "'"+policeBoxList.policeContents[i].getPoliceBoxPlace()+"',"+policeBoxList.policeContents[i].getLatitude()+","
                    +policeBoxList.policeContents[i].getLongitude()+",'"+policeBoxList.policeContents[i].getPoliceBoxName()+"','"+policeBoxList.policeContents[i].getPoliceBoxPhoneNumber()+"');");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
