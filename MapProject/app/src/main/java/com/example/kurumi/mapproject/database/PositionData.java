package com.example.kurumi.mapproject.database;

import java.io.Serializable;

/**
 * Created by kurumi on 2015/08/18.
 */
public class PositionData implements Serializable{
    double lat;
    double lng;
    String title;
    String place;
    String date;
    String content;
    String men;
    boolean incident_or_policeBox;
    private float color;


    public PositionData(double lat, double lng, String title,String place,String date,String content,String men,float color) {
        this.lat = lat;
        this.lng = lng;
        this.title = title;
        this.place=place;
        this.date=date;
        this.content=content;
        this.men=men;
        this.color=color;
    }

    public boolean isIncident_or_policeBox() {
        return incident_or_policeBox;
    }

    public void setIncident_or_policeBox(boolean incident_or_policeBox) {
        this.incident_or_policeBox = incident_or_policeBox;
    }




    public float getColor() {
        return color;
    }

    public void setColor(float color) {
        this.color = color;
    }
    public PositionData(){
    }


    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public String getTitle() {
        return title;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
