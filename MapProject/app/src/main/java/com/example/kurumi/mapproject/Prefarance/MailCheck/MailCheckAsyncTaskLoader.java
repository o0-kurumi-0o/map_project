package com.example.kurumi.mapproject.Prefarance.MailCheck;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import java.util.Properties;

import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;

/**
 * Created by kurumi on 2015/08/28.
 */
public class MailCheckAsyncTaskLoader extends AsyncTaskLoader<Boolean>{

    String user;
    String password;
    String port;
    String host;
    boolean mData=false;


    public MailCheckAsyncTaskLoader(Context context, String data1, String data2, String data3, String data4) {
        super(context);
        this.user=data1;
        this.password=data2;
        this.port=data3;
        this.host=data4;
    }

    @Override
    public Boolean loadInBackground() {
        /*

        boolean result = false;
        String auth="false";
        try {
            Properties props = new Properties();

            Session session = Session.getInstance(props, null);
            Transport transport = session.getTransport("smtp");
            int portint = Integer.parseInt(port);
            transport.connect(host, portint, user, password);
            transport.close();
            result = true;

        } catch(AuthenticationFailedException e) {
         //   new Log(e.toString(), "SMTP: Authentication Failed", false, true);

        } catch(MessagingException e) {
        //    Logging.addMsg(e.toString(), "SMTP: Messaging Exception Occurred", false, true);
        } catch (Exception e) {
        //    Logging.addMsg(e.toString(), "SMTP: Unknown Exception", false, true);
        }
        System.out.println("user "+user);
        System.out.println("pass "+password);
        System.out.println("port "+port);
        System.out.println("imap "+host);


        System.out.println("aaaaaaaaaaaaaaaaaaa   "+result);

        return result;
        */
        Store st = null;
        Properties props = System.getProperties();
        Session sess = Session.getInstance(props, null);
        try {
            st = sess.getStore("imaps");
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        boolean result = false;
        String auth="false";
        try {

            int portint = Integer.parseInt(port);

            assert false;
            st.connect(host, portint, user, password);

            result = true;

        } catch (Exception e) {
            //    Logging.addMsg(e.toString(), "SMTP: Unknown Exception", false, true);
        }
        return result;
    }

    @Override
    protected void onStartLoading() {

            // takeContentChanged いまいちよく分からない
            // CursorLoaderの時に意味があるのかも…

            // 非同期処理を開始
            forceLoad();

    }

    @Override
    protected void onStopLoading() {
        // Loader停止時の処理

        // 非同期処理のキャンセル
        cancelLoad();
    }

    @Override
    public void deliverResult(Boolean data) {
        // 登録してあるリスナー（LoaderCallbacksを実装したクラス）に結果を送信する

        if (isReset()) {
            // Loaderがリセット状態かどうか
            // trueの場合はLoaderがまだ一度も開始していない、resetメソッドが呼ばれている
            return;
        }

        mData = data;

        super.deliverResult(data);
    }

    @Override
    protected void onReset() {
        // reset呼び出し時、Loader破棄時の処理
        super.onReset();

        // Loaderを停止
        onStopLoading();

        // データをクリア
    }

}
