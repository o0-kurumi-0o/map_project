package com.example.kurumi.mapproject.AsyncTask;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.content.AsyncTaskLoader;

import com.example.kurumi.mapproject.database.ContentData;
import com.example.kurumi.mapproject.database.PositionData;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by kurumi on 2015/08/18.
 */
public class MarkerAsyncTaskLoader extends AsyncTaskLoader<ArrayList<PositionData>>{
    private ArrayList<ContentData> arrayList=new ArrayList<>();
    private ArrayList<ContentData> sub = new ArrayList<>();
    private ArrayList<PositionData> positionDatas = new ArrayList<>();

    private HashMap<Marker, PositionData> eventMarkerMap;
    private Context mContext;
    private Float[] markerColor={BitmapDescriptorFactory.HUE_YELLOW,BitmapDescriptorFactory.HUE_GREEN,BitmapDescriptorFactory.HUE_RED,BitmapDescriptorFactory.HUE_MAGENTA};


    public MarkerAsyncTaskLoader(Context context) {
        super(context);
        this.mContext=context;
    }

    public MarkerAsyncTaskLoader(Context context, ArrayList<ContentData> arrayList) {
        super(context);
        this.mContext=context;
        this.arrayList = arrayList;
    }

    @Override
    public ArrayList<PositionData> loadInBackground() {
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        for (int i = 0; i < arrayList.size(); i++) {
            sub.add(new ContentData(
                    arrayList.get(i).getTime(), all_place_content(arrayList.get(i).getPlace()), arrayList.get(i).getContent(),
                    arrayList.get(i).getMen(), markerColor[arrayList.get(i).getCategory()]));
            try {
                PositionData w = changeFormat(geocoder, i);
                if(w!=null){
                    w.setIncident_or_policeBox(true);
                    positionDatas.add(w);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return positionDatas;
    }


    private String all_place_content(String place) {
        //左分解
        int index_left = place.indexOf("：");
        if (index_left > 1) {
            place = place.substring(index_left + 1);
        }
        //右分解
        int index_right_1 = place.indexOf("の");

        if (index_right_1 > 1) {
            place = place.substring(0, index_right_1);
        }
        int index_right_2 = place.indexOf("付近");
        if (index_right_2 > 1) {
            place = place.substring(0, index_right_2);
        }
        return place;
    }

    private PositionData changeFormat(Geocoder geocoder, int position) throws IOException {
        List<Address> addressList = geocoder.getFromLocationName(sub.get(position).getPlace(), 1);
        if(addressList.size()!=0){
            Address address = addressList.get(0);
            double lat = address.getLatitude();
            double lng = address.getLongitude();
            return new PositionData(lat, lng, sub.get(position).getPlace(),
                    sub.get(position).getPlace(),sub.get(position).getTime(),sub.get(position).getContent(),sub.get(position).getMen(),sub.get(position).getColor());
        }
        return null;
    }

}
