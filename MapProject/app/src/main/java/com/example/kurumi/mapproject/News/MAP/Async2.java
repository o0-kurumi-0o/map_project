package com.example.kurumi.mapproject.News.MAP;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;

import com.example.kurumi.mapproject.R;
import com.example.kurumi.mapproject.database.ContentData;
import com.example.kurumi.mapproject.database.PositionData;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by kurumi on 2015/07/12.
 */
public class Async2 extends AsyncTask<Void, Void, Void> {

    private HashMap<Marker, ContentData> eventMarkerMap;
    private Context mContext;
    private GoogleMap map;
    private ContentData contentData;
    private ContentData sub;
    private PositionData positionDatas;

    public Async2(Context mContext, GoogleMap mMap, ContentData contentData, HashMap<Marker, ContentData> eventMarkerMap) {
        this.mContext = mContext;
        this.map = mMap;
        this.contentData = contentData;
        this.eventMarkerMap=eventMarkerMap;
    }


    @Override
    protected Void doInBackground(Void... params) {
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
            sub=new ContentData(contentData.getCategory(), contentData.getTime(), all_place_content(contentData.getPlace()),
                    contentData.getContent(), contentData.getMen() );
            try {
                PositionData aa = changeFormat(geocoder, 0);
                if(aa!=null) {
                    positionDatas=aa;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        return null;
    }

    private PositionData changeFormat(Geocoder geocoder, int position) throws IOException {

        List<Address> addressList = geocoder.getFromLocationName(sub.getPlace(), 1);
        if(addressList.size()!=0){
        Address address = addressList.get(0);
        double lat = address.getLatitude();
        double lng = address.getLongitude();
        return new PositionData(lat, lng, sub.getPlace(),sub.getPlace(),sub.getTime(),sub.getContent()
        ,sub.getMen(),sub.getColor());
    }
        return null;
    }

    private String all_place_content(String place) {

        //左分解
        int index_left = place.indexOf("：");
        if (index_left > 1) {
            place = place.substring(index_left + 1);
        }
        //右分解
        int index_right_1 = place.indexOf("の");
        int index_right_2 = place.indexOf("付近");
        if (index_right_1 > 1) {
            place = place.substring(0, index_right_1);
        }
        if (index_right_2 > 1) {
            place = place.substring(0, index_right_2);
        }
        return place;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

            Marker m= map.addMarker(new MarkerOptions().position(new LatLng(positionDatas.getLat(), positionDatas.getLng())).
                    title(positionDatas.getTitle()));
            eventMarkerMap.put(m, sub);
            CircleOptions circleOptions = new CircleOptions()
                    .center(new LatLng(positionDatas.getLat(), positionDatas.getLng()))
                    .radius(100).fillColor(mContext.getResources().getColor(R.color.map_circle)).strokeColor(mContext.getResources().getColor(R.color.map_circle));
            map.addCircle(circleOptions);
        if(positionDatas!=null) {
            CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(new LatLng(positionDatas.getLat(), positionDatas.getLng()), 15);
            map.moveCamera(cu);
        }
    }
}