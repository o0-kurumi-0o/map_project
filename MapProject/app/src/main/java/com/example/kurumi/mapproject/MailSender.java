package com.example.kurumi.mapproject;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;

import com.example.kurumi.mapproject.database.ContentDAO;
import com.example.kurumi.mapproject.database.ContentData;
import com.example.kurumi.mapproject.database.MySQLiteOpenHelper;
import com.sun.mail.imap.IMAPFolder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.mail.FetchProfile;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.SearchTerm;

/**
 * Created by kurumi on 2015/07/12.
 * メールに利用しているJavamailが現時点で使いこなせていない
 * とりあえず速度が遅いので、後ほど修正
 *
 */
public class MailSender {
    static public final String DATE_PATTERN = "yyyy/MM/dd HH:mm:ss";
    String host = "imap.gmail.com";
    int port = 993;
    String user = "sample.javamail@gmail.com";
    String password = "h19931024";
    String target_folder = "INBOX";
    String encode[]=new String[]{"痴漢発生","わいせつ","不審者","子供安全"};

    private Properties properties;
    private Context context;
    Store store = null;
    Session session;
    Properties props;
    private SQLiteDatabase db;
    private ArrayList<ContentData> mailContent = new ArrayList<>();


    public MailSender(Context context) {
        properties = System.getProperties();
        this.context = context;
    }


    public void getMailContent() {

        Properties props = System.getProperties();
        Session sess = Session.getInstance(props, null);


        //prefarencから取得
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        final String str = sp.getString("SaveDate", "");
        user= sp.getString("user", "");
        password = sp.getString("password", "");
        port =Integer.valueOf(sp.getString("port", "0"));
        host = sp.getString("host", "");

        //範囲設定
        SearchTerm searchCondition = new SearchTerm() {
            @Override
            public boolean match(Message message) {
                try {
                    //prefarence確認
                    if (str.length() == 0 || str.equals("")) {
                        return(message.getSubject().contains(encode[0])||message.getSubject().contains(encode[1]) ||message.getSubject().contains(encode[2]) || message.getSubject().contains(encode[3]));
                        //無い場合
                    } else {
                        SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
                        // Date型変換
                        Date formatDate = sdf.parse(str);
                        int diff = formatDate.compareTo(message.getSentDate());
                        //(message.getSubject().contains("子供安全情報") || message.getSubject().contains("痴漢")) && diff <= 0;
                    //    return (message.getSubject().contains("痴漢")) && diff <= 0;
                        return (message.getSubject().contains(encode[0])||message.getSubject().contains(encode[1]) || message.getSubject().contains(encode[2])|| message.getSubject().contains(encode[3])) && diff <= 0;
                    }

                } catch (MessagingException | ParseException ex) {
                    ex.printStackTrace();
                }
                return false;
            }
        };

        Store st = null;
        try {
            st = sess.getStore("imaps");
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        try {
            assert st != null;
            st.connect(host, port, user, password);

            Folder fol = st.getFolder(target_folder);

            if (fol.exists()) {
                fol.open(Folder.READ_ONLY);

                Message[] foundMessages = fol.search(searchCondition);
                //         Message[] foundMessages =fol.getMessages();
                FetchProfile fp = new FetchProfile();
                fp.add(FetchProfile.Item.ENVELOPE);
                fp.add(IMAPFolder.FetchProfileItem.FLAGS);
                fp.add(IMAPFolder.FetchProfileItem.CONTENT_INFO);
                fp.add("X-mailer");
                fol.fetch(foundMessages, fp);
                Date date = new Date();
                System.out.println(foundMessages.length);
             //   for (Message m : foundMessages) {
                    for(int i=0;i<foundMessages.length;i++){
                    //メッセージループ
System.out.println("vaio");

                    Object content = foundMessages[i].getContent();
                    if (foundMessages[i].isMimeType("text/plain")) {
                        String[] split_content = foundMessages[i].getContent().toString().split("[◆◎]");
                        //必要な情報だけに分解する
                        if (split_content.length >= 4) {
                            mailContent.add(new ContentData(encodeCategory(foundMessages[i].getSubject()),
                                    time_format(split_content[1].replaceAll("\n", ""),foundMessages[i].getReceivedDate()),
                                    split_content[2].replaceAll("\n", ""), split_content[3].replaceAll("\n", ""), split_content[4].replaceAll("\n", "")));
                        }
                    }
                    //     mailContent.add(new ContentData(String.valueOf(m.getReceivedDate()),m.getSubject(),m.getInputStream().toString()));
                    //          System.out.println("通過");

                    //      System.out.printf("確認"+"%s - %d\n", m.getSubject(), m.getSize());
                }
                MySQLiteOpenHelper dbHelper = new MySQLiteOpenHelper(context);
                db = dbHelper.getWritableDatabase();
                ContentDAO contentDAO = new ContentDAO(db, dbHelper);
                contentDAO.insert(mailContent);
                System.out.println("vita");
                fol.close(false);


            }

            st.close();

            //現在の時間をprefaranceに保存
            SharedPreferences spp = PreferenceManager.getDefaultSharedPreferences(context);
            spp.edit().putString("SaveDate", new SimpleDateFormat(DATE_PATTERN).format(new Date())).apply();


        } catch (MessagingException | IOException e) {
            e.printStackTrace();
            System.out.println("っらー");
        }
    }

    private int encodeCategory(String subject) {
        for(int i=0;i<=encode.length;i++){
            if(subject.matches(".*" + encode[i] + ".*")){
                return i;
            }
        }
        return 0;
    }


    //フォーマットを整える
    private String time_format(String s, Date receivedDate) {

        Calendar datetime = Calendar.getInstance();
        datetime.setTime(receivedDate);
        int index = s.indexOf("年");
        if(index!=-1){
            s="日時："+s.substring(index+1);
        }
        s = s.replace("本日", String.valueOf(datetime.get(Calendar.MONTH)+1)+"月"+String.valueOf(datetime.get(Calendar.DAY_OF_MONTH))+"日");
        datetime.add(Calendar.DAY_OF_MONTH,-1);
        s = s.replace("昨日", String.valueOf(datetime.get(Calendar.MONTH)+1)+"月"+String.valueOf(datetime.get(Calendar.DAY_OF_MONTH))+"日");

        StringBuilder sb = new StringBuilder(s);
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if ('０' <= c && c <= '９') {
                sb.setCharAt(i, (char) (c - '０' + '0'));
            }
        }
        sb.insert(3, String.valueOf(datetime.get(Calendar.YEAR)) + "年");

      //  String w=sb.toString();


        return sb.toString();
    }




    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }
}