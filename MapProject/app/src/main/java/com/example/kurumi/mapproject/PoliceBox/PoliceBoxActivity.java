package com.example.kurumi.mapproject.PoliceBox;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.kurumi.mapproject.R;

public class PoliceBoxActivity extends AppCompatActivity {

    private PoliceBoxActivityFragment policeBoxFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_police_box);
        setupToolBar();
        TabLayout tabLayout = (TabLayout)findViewById(R.id.tab_layout);
        if (savedInstanceState == null) {
            policeBoxFragment = new PoliceBoxActivityFragment();
 //           getSupportFragmentManager().beginTransaction().add(R.id.mainFrame, policeBoxFragment).commit();
        }

        ViewPager pager = (ViewPager) findViewById(R.id.Pager);

        FragmentManager fm = getSupportFragmentManager();
        FragmentPagerAdapter fragmentPagerAdapter = new FragmentPagerAdapter(fm,policeBoxFragment);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        pager.setAdapter(fragmentPagerAdapter);
        tabLayout.setupWithViewPager(pager);

    }



    private void setupToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);

        toolbar.setTitle("交番情報");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_up);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_police_box, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
