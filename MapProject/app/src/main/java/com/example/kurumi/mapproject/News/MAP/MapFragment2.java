package com.example.kurumi.mapproject.News.MAP;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kurumi.mapproject.R;
import com.example.kurumi.mapproject.database.ContentData;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment2 extends Fragment{
    private GoogleMap mMap; //,.
    private View view;
    private TextView textViewTime;
    private TextView textViewPlace;
    private TextView textViewContent;
    private ContentData contentData;
    private HashMap<Marker,ContentData> eventMarkerMap= new HashMap<>();

    public MapFragment2() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState==null) {
            contentData =((ContentData) getArguments().getSerializable("content"));
        }
        }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        view=rootView.findViewById(R.id.alertLayout);

        textViewPlace =(TextView)rootView.findViewById(R.id.textView);
        textViewTime =(TextView)rootView.findViewById(R.id.textView3);
        textViewContent =(TextView)rootView.findViewById(R.id.textView2);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
                Async2 task = new Async2(getActivity(),mMap, contentData,eventMarkerMap);
                task.execute();
        //        setUpMap_from_DB();

            }
        }
    }

    private void setUpMap() {
        // latitude and longitude


        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng arg0) {
                view.setVisibility(View.INVISIBLE);
            }
        });


        textViewPlace.setText(contentData.getPlace());
        textViewTime.setText(contentData.getTime());
        textViewContent.setText(contentData.getContent());
        view.setVisibility(View.VISIBLE);


        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                ContentData eventInfo = eventMarkerMap.get(marker);
                textViewPlace.setText(eventInfo.getPlace());
                textViewTime.setText(eventInfo.getTime());
                textViewContent.setText(eventInfo.getContent());
                view.setVisibility(View.VISIBLE);
                return false;
            }
        });
    }
}
