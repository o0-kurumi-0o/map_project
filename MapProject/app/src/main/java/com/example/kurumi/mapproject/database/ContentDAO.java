package com.example.kurumi.mapproject.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.example.kurumi.mapproject.PoliceBox.PoliceContent;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by kurumi on 2015/03/24.
 * データベースへのデータ挿入、更新操作を行う
 * データアクセスオブジェクトとしてカプセル化することが目的
 */
public class ContentDAO {
    private static String table_map="table_map";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_CATEGORY = "category";
    private static final String COLUMN_TIME = "time";
    private static final String COLUMN_PLACE = "place";
    private static final String COLUMN_CONTENT = "content";
    private static final String COLUMN_MEN = "men";
    private static final String[] COLUMNS =
            {COLUMN_ID,COLUMN_CATEGORY, COLUMN_TIME, COLUMN_PLACE, COLUMN_CONTENT,COLUMN_MEN};

    private static String table_police="table_policeBox";
    private static final String COLUMN_WARD = "ward";
    private static final String COLUMN_LAT = "lat";
    private static final String COLUMN_LON = "lon";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_PHONE = "phone";
    private static final String[] COLUMNS_P =
            {COLUMN_ID,COLUMN_WARD,COLUMN_PLACE,COLUMN_LAT, COLUMN_LON, COLUMN_NAME, COLUMN_PHONE};

    private SQLiteDatabase db;

    public ContentDAO(SQLiteDatabase dbHelper, MySQLiteOpenHelper dbHelper1) {
        this.db = dbHelper;
    }


    public void insert(ArrayList<ContentData> contentData) {
        db.beginTransaction();
        try {
            SQLiteStatement stmt = db.compileStatement(
                    "INSERT INTO table_map(" +
                            "category,time,place,content,men)" +
                            " VALUES(?,?,?,?,?)");
            for (int i = 0; i < contentData.size(); i++) {
                stmt.bindLong(1, contentData.get(i).getCategory());
                stmt.bindString(2, contentData.get(i).getTime());
                stmt.bindString(3, contentData.get(i).getPlace());
                stmt.bindString(4, contentData.get(i).getContent());
                stmt.bindString(5, contentData.get(i).getMen());
                stmt.executeInsert();
                //   db.setTransactionSuccessful();
            }
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
        }
    }

    public void insert(ContentData contentData) {
        db.beginTransaction();
        try {
            SQLiteStatement stmt = db.compileStatement(
                    "INSERT INTO table_map(" +
                            "category,time,place,content,men)" +
                            " VALUES(?,?,?,?,?)");
                stmt.bindLong(1, contentData.getCategory());
                stmt.bindString(2, contentData.getTime());
                stmt.bindString(3, contentData.getPlace());
                stmt.bindString(4, contentData.getContent());
                stmt.bindString(5, contentData.getMen());
                stmt.executeInsert();
                //   db.setTransactionSuccessful();
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
        }
    }


    public ArrayList<ContentData> getAll(){
        ArrayList<ContentData> dbDataList = new ArrayList<>();
        String DESC = " DESC";
        Cursor cursor =
                db.query(table_map, COLUMNS, null, null, null, null, COLUMN_ID + DESC);
        while (cursor.moveToNext()) {

            ContentData dbData = new ContentData();
            dbData.setCategory(cursor.getInt(1));
            dbData.setTime(cursor.getString(2));
            dbData.setPlace(cursor.getString(3));
            dbData.setContent(cursor.getString(4));
            dbData.setMen(cursor.getString(5));
            dbDataList.add(dbData);
        }
        return dbDataList;
    }


    public ArrayList<ContentData> selectNews(int position){
        //position=1    痴漢
        //position=2    わいせつ
        //position=3    子供
        //0はありれない
 //       String sql="category =="+(position);

        ArrayList<ContentData> dbDataList = new ArrayList<>();
   //     Cursor cursor = db.query(table_map, COLUMNS,sql, null, null, null, COLUMN_ID + DESC);

        String sql="SELECT * FROM table_map WHERE category= ? ORDER BY _id DESC";
        Cursor cursor = db.rawQuery(sql,new String[]{String.valueOf(position)});

        while (cursor.moveToNext()) {

            ContentData dbData = new ContentData();
            dbData.setCategory(cursor.getInt(1));
            dbData.setTime(cursor.getString(2));
            dbData.setPlace(cursor.getString(3));
            dbData.setContent(cursor.getString(4));
            dbData.setMen(cursor.getString(5));
            dbDataList.add(dbData);
        }
        return dbDataList;
    }



    public String extractMatchString(String regex, String target) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(target);

        System.out.println(regex);
        System.out.println(target);
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            throw new IllegalStateException("No match found.");
        }
    }


    //検索

    //処理が遅いので見直し必須
    public ArrayList<ContentData> getMolester(Boolean[] booleans, int selectedItemPosition){

        int[] period={7,31,365};
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date formatDate = null;
        StringBuilder sb = new StringBuilder();

        ArrayList<String> args=new ArrayList<>();

        for(int i=0;i<booleans.length;i++) {
           if(booleans[i]){
               args.add(String.valueOf(i));
               if(sb.length()==0){
                   sb.append("category == ?");
               }else {
                   sb.append(" OR " + "category == ?");
               }
           }
        }
        ArrayList<ContentData> dbDataList = new ArrayList<>();
        Date date = new Date();
  //      Cursor cursor = db.query(table_map, COLUMNS, selection, null, null, null, COLUMN_ID + DESC);

        String sql="SELECT * FROM table_map WHERE "+sb.toString()+" ORDER BY _id DESC";
        Cursor cursor = db.rawQuery(sql, args.toArray(new String[args.size()]));

        while (cursor.moveToNext()) {
            ContentData dbData = new ContentData();
            dbData.setCategory(cursor.getInt(1));
            dbData.setTime(cursor.getString(2));
            dbData.setPlace(cursor.getString(3));
            dbData.setContent(cursor.getString(4));
            dbData.setMen(cursor.getString(5));
            dbDataList.add(dbData);
        }



        //ここ遅いと思う
        //全てのものを取得して、それから期間外のものを消す

        if(selectedItemPosition!=3) {
            for (int i = dbDataList.size() - 1; i >= 0; i--) {
                String time = dbDataList.get(i).getTime();
                String regex_year = "\\：(.+)\\年";
                String regex_month = "\\年(.+)\\月";
                String regex_day = "\\月(.+)\\日";
                String year = extractMatchString(regex_year, time);
                String month = extractMatchString(regex_month, time);
                String day = extractMatchString(regex_day, time);

                if (month.length() == 1) {
                    month = "0" + month;
                }
                if (day.length() == 1) {
                    day = "0" + day;
                }
                String dateStr = year + month + day;
                // Date型変換
                try {
                    formatDate = sdf.parse(dateStr);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                // 日付をlong値に変換します。
                long dateTimeTo = date.getTime();
                long dateTimeFrom = formatDate.getTime();

                // 差分の日数を算出します。
                long dayDiff = (dateTimeTo - dateTimeFrom) / (1000 * 60 * 60 * 24);
                if(dayDiff>period[selectedItemPosition]){
                 dbDataList.remove(i);
                }
            }
        }

        return dbDataList;
    }

    public ArrayList<PoliceContent> getPoliceBox(int position) {
        ArrayList<PoliceContent> policeContents=new ArrayList<>();

  //      String selection = "ward = " + position;
        //    String DESC = " DESC";
        //    Cursor cursor = db.query(table_police, COLUMNS_P, selection, null, null, null, COLUMN_ID + DESC);

        String sql="SELECT * FROM table_policeBox WHERE ward= ? ORDER BY _id DESC";
        Cursor cursor = db.rawQuery(sql,new String[]{String.valueOf(position)});

            while (cursor.moveToNext()) {
                PoliceContent dbData = new PoliceContent();
                dbData.setPoliceBoxTag(cursor.getInt(1));
                dbData.setPoliceBoxPlace(cursor.getString(2));
                dbData.setLatitude(cursor.getDouble(3));
                dbData.setLongitude(cursor.getDouble(4));
                dbData.setPoliceBoxName(cursor.getString(5));
                dbData.setPoliceBoxPhoneNumber(cursor.getString(6));

                policeContents.add(dbData);
            }
        return policeContents;
    }

    public PositionData getNearestPoliceBox(String[] lat_long) {

        String sql="SELECT * FROM `table_policeBox` ORDER BY ABS(`lat` - ?) + ABS(`lon` - ?) ASC";
        Cursor cursor =
                db.rawQuery(sql, lat_long);
        PositionData positionData= new PositionData();
        if (cursor.moveToNext()) {
            positionData.setTitle(cursor.getString(cursor.getColumnIndex("name")));
            positionData.setLat(cursor.getDouble(cursor.getColumnIndex("lat")));
            positionData.setLng(cursor.getDouble(cursor.getColumnIndex("lon")));
            positionData.setPlace(cursor.getString((cursor.getColumnIndex("place"))));
            positionData.setContent(cursor.getString((cursor.getColumnIndex("phone"))));
        }
        positionData.setIncident_or_policeBox(false);
        return positionData;
    }
}
