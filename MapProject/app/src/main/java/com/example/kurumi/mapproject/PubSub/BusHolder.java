package com.example.kurumi.mapproject.PubSub;

import com.squareup.otto.Bus;

/**
 * Created by kurumi on 2015/09/11.
 */
public class BusHolder {

    private static Bus sBus = new Bus();

    public static Bus get() {
        return sBus;
    }

}