package com.example.kurumi.mapproject.Splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.example.kurumi.mapproject.Map.MapActivity;
import com.example.kurumi.mapproject.R;

public class SplashActivity extends AppCompatActivity {

    private Handler handler;
    private Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                // MainActivityを呼び出す
                Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                // アクティビティ(MainActivity)を起動する
                startActivity(intent);
                // SplashActivityを終了する
                SplashActivity.this.finish();
            }
        };

        handler.postDelayed(runnable,1000); // 2000ミリ秒後（2秒後）に実行
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }
}
