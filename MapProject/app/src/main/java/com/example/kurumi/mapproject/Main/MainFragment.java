package com.example.kurumi.mapproject.Main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.kurumi.mapproject.Alert.AlertActivity;
import com.example.kurumi.mapproject.AsyncTask.AsyncHttpRequest;
import com.example.kurumi.mapproject.Dialog.ErrorShowDialogFragment;
import com.example.kurumi.mapproject.Map.MapActivity;
import com.example.kurumi.mapproject.News.NewsActivity;
import com.example.kurumi.mapproject.PoliceBox.PoliceBoxActivity;
import com.example.kurumi.mapproject.Prefarance.SettingsActivity;
import com.example.kurumi.mapproject.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends Fragment {

    private Button MapButton;
    private Button AlertButton;
    private Button newsButton;
    private Button settingButton;
    private Button button;
    private Button policeBoxButton;

    public MainFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        MapButton = (Button) rootView.findViewById(R.id.button1);
        AlertButton = (Button) rootView.findViewById(R.id.button2);

        button = (Button) rootView.findViewById(R.id.button);
        newsButton = (Button) rootView.findViewById(R.id.button3);
        settingButton = (Button) rootView.findViewById(R.id.button4);
        policeBoxButton=(Button)rootView.findViewById(R.id.button6);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        MapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MapActivity.class);
                startActivity(intent);
            }
        });
        AlertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AlertActivity.class);
                startActivity(intent);
            }
        });
        newsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NewsActivity.class);
                startActivity(intent);
            }
        });
        settingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SettingsActivity.class);
                startActivity(intent);
            }
        });

        policeBoxButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PoliceBoxActivity.class);
                startActivity(intent);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
                String a = sharedPref.getString("user", "");
                String b = sharedPref.getString("password", "");
                String c = sharedPref.getString("port", "");
                String d = sharedPref.getString("host", "");
                if (a.length() != 0 && b.length() != 0 && c.length() != 0 && d.length() != 0) {
                    AsyncHttpRequest task = new AsyncHttpRequest(getActivity());
                    task.execute();
                }else {
                    ErrorShowDialogFragment errorShowDialogFragment = new ErrorShowDialogFragment();
                    Bundle message = new Bundle();
                    message.putString("title", getResources().getString(R.string.error_title));
                    message.putString("message","メールを設定してください");
                    errorShowDialogFragment.setArguments(message);
                    errorShowDialogFragment.show(getChildFragmentManager(), "error");
                }
            }
        });

    }


}
