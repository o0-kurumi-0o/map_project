package com.example.kurumi.mapproject.Dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;

import com.example.kurumi.mapproject.Map.MapDialogState;
import com.example.kurumi.mapproject.R;

/**
 * Created by kurumi on 2015/08/24.
 */
public class MapSettingChangeDialogFragment extends DialogFragment{
    CheckBox checkBox1;
    CheckBox checkBox2;
    CheckBox checkBox3;
    private CheckBox checkBox4;
    private OnOkClickListener mListener;
    private Spinner spinner;
    private MapDialogState mapDialogState;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mapDialogState= (MapDialogState) getArguments().getSerializable("state");
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_map_setting, null);

        spinner=(Spinner)rootView.findViewById(R.id.spinner2);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // アイテムを追加します
        adapter.add("1週間以内");
        adapter.add("1ヶ月以内");
        adapter.add("1年以内");
        adapter.add("すべての期間");
        spinner.setAdapter(adapter);

        checkBox1=(CheckBox)rootView.findViewById(R.id.checkBox1);
        checkBox2=(CheckBox)rootView.findViewById(R.id.checkBox2);
        checkBox3=(CheckBox)rootView.findViewById(R.id.checkBox3);
        checkBox4=(CheckBox)rootView.findViewById(R.id.checkBox4);


        //Viewの設定
        spinner.setSelection(mapDialogState.getPeriod());
        checkBox1.setChecked(mapDialogState.isCheck1());
        checkBox2.setChecked(mapDialogState.isCheck2());
        checkBox3.setChecked(mapDialogState.isCheck3());
        checkBox4.setChecked(mapDialogState.isCheck4());


        builder.setView(rootView);
        builder.setNegativeButton("キャンセル",

                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                }
        ).setPositiveButton("決定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onOkClicked(spinner.getSelectedItemPosition(),checkBox1.isChecked(),checkBox2.isChecked(),checkBox3.isChecked(),checkBox4.isChecked());
            }
        });
        return builder.create();
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        try {
            mListener = (OnOkClickListener)activity;
        } catch (ClassCastException ignored) {
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnOkClickListener {
        void onOkClicked(int selectedItemPosition,Boolean checkBox1, Boolean checkBox2, Boolean checkBox3,Boolean checkBox4);
    }

}
