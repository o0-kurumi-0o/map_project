package com.example.kurumi.mapproject.database;

import java.io.Serializable;

/**
 * Created by kurumi on 2015/07/27.
 */
public class ContentData implements Serializable {
    private String time;
    private String place;
    private String content;
    //0 ちかん　
    //1 わいせつ
    //2 子供
    private int category;
    private String men;
    private float color;

    public float getColor() {
        return color;
    }

    public void setColor(float color) {
        this.color = color;
    }



    public String getMen() {
        return men;
    }

    public void setMen(String men) {
        this.men = men;
    }



    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }



    public ContentData() {

    }

    public ContentData(int category, String time, String place, String content, String men) {
        this.category = category;
        this.time = time;
        this.place = place;
        this.content = content;
        this.men = men;
    }

    public ContentData(String time, String place, String content, String men, float color) {
        this.time = time;
        this.place = place;
        this.content = content;
        this.men = men;
        this.color = color;
    }

    public String getTime() {
        return time;
    }

    public String getPlace() {
        return place;
    }

    public String getContent() {
        return content;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public void setContent(String content) {
        this.content = content;
    }


}
