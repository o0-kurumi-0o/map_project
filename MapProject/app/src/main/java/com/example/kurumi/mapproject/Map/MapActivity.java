package com.example.kurumi.mapproject.Map;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.kurumi.mapproject.Alert.AlertActivity;
import com.example.kurumi.mapproject.Dialog.MapSettingChangeDialogFragment;
import com.example.kurumi.mapproject.News.NewsActivity;
import com.example.kurumi.mapproject.PoliceBox.PoliceBoxActivity;
import com.example.kurumi.mapproject.Prefarance.SettingsActivity;
import com.example.kurumi.mapproject.PubSub.BusHolder;
import com.example.kurumi.mapproject.PubSub.MapTitleChange;
import com.example.kurumi.mapproject.R;
import com.squareup.otto.Subscribe;

public class MapActivity extends AppCompatActivity implements MapSettingChangeDialogFragment.OnOkClickListener {

    private MapFragment mapFragment;
    private Toolbar toolbar;
    private String title;

    private DrawerLayout dlDrawer;
    private ActionBarDrawerToggle drawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        setupToolBar();
        setupDrawer();
        NavigationView nvDrawer = (NavigationView) findViewById(R.id.nvView);
        setupDrawerContent(nvDrawer);


        if (savedInstanceState == null) {
            title="今週の犯罪";
            mapFragment = new MapFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.mainFrame, mapFragment).commit();
        }
    }

    private void setupDrawer() {
        // Find our drawer view
        dlDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = setupDrawerToggle();

        // Tie DrawerLayout events to the ActionBarToggle
        dlDrawer.setDrawerListener(drawerToggle);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.nav_first_fragment:
                break;

            case R.id.nav_second_fragment:

                //現在は面倒なのでこの処理で放置

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent;
                        intent = new Intent(MapActivity.this, AlertActivity.class);
                        startActivity(intent);
                    }
                }, 250);

                break;

            case R.id.nav_third_fragment:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent;
                        intent = new Intent(MapActivity.this, NewsActivity.class);
                        startActivity(intent);
                    }
                }, 250);


                break;
            case R.id.nav_forth_fragment:

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent;
                        intent = new Intent(MapActivity.this, PoliceBoxActivity.class);
                        startActivity(intent);
                    }
                }, 250);
                break;

            case R.id.nav_menu_fragment:

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent;
                        intent = new Intent(MapActivity.this, SettingsActivity.class);
                        startActivity(intent);
                    }
                }, 250);
                break;

        }


        // Highlight the selected item, update the title, and close the drawer
        //   menuItem.setChecked(true);
        //   setTitle(menuItem.getTitle());
        dlDrawer.closeDrawers();
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, dlDrawer, toolbar, R.string.app_name, R.string.app_name);
    }

    @Override
    public void onBackPressed() {
        if (dlDrawer.isDrawerOpen(Gravity.LEFT)) {
            dlDrawer.closeDrawers();
        } else {
            super.onBackPressed();
        }
    }


    private void setupToolBar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        assert ab != null;
        ab.setHomeAsUpIndicator(R.drawable.ic_drawer);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_filter) {
            mapFragment.showSettingDialog();
            return true;
        }
        if (id == R.id.action_here) {
            mapFragment.my_location();
            return true;
        }
        if (id == R.id.action_police_box) {
            mapFragment.nearest_police_box();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();
        // Subscriberとして登録する
        BusHolder.get().register(this);

    }

    @Override
    protected void onPause() {
        super.onPause();
        // Subscriberの登録を解除する
        BusHolder.get().unregister(this);
    }


    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("title", title);
        if (mapFragment != null) {
            getSupportFragmentManager().putFragment(outState, "mapFragment", mapFragment);
        }
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        title=savedInstanceState.getString("title");
        if (mapFragment == null) {
            mapFragment = (MapFragment) getSupportFragmentManager().getFragment(savedInstanceState, "mapFragment");
        }
    }

    @Override
    public void onOkClicked(int selectedItemPosition, Boolean checkBox1, Boolean checkBox2, Boolean checkBox3, Boolean checkBox4) {
        mapFragment.changePin(selectedItemPosition, checkBox1, checkBox2, checkBox3, checkBox4);
    }

    @Subscribe
    public void subscribe(MapTitleChange event) {
        title = event.title;
        toolbar.setTitle(event.title);
    }

}
