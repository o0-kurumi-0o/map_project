package com.example.kurumi.mapproject.PoliceBox.content;

import com.example.kurumi.mapproject.PoliceBox.PoliceContent;

/**
 * Created by kurumi on 2015/09/07.
 */
public class policeBoxList {
    public final static PoliceContent[] policeContents = new PoliceContent[]{

            new PoliceContent(0, "川端警察署", "京都市左京区岡崎徳成町１", "075-771-0110", 35.015065, 135.778094),
            new PoliceContent(0, "岡崎公園交番", "京都市左京区岡崎成勝寺町７１", "075-771-0166", 35.013429, 135.78226),
            new PoliceContent(0, "南禅寺交番", "京都市左京区南禅寺草川町３７", "075-771-0033", 35.011293, 135.787937),
            new PoliceContent(0, "黒谷交番", "京都市左京区岡崎東福ノ川町３２", "075-771-0032", 35.018394, 135.785731),
            new PoliceContent(0, "東一条交番", "京都市左京区吉田中阿達町３７－１", "075-771-0169", 35.025116, 135.776398),
            new PoliceContent(0, "銀閣寺交番", "京都市左京区銀閣寺前町５７", "075-771-0167", 35.027739, 135.792765),

            new PoliceContent(1, "上京警察署", "京都市上京区御前通今小路下る馬喰町６９２－１", "075-465-0110", 35.027628, 135.736202),
            new PoliceContent(1, "今出川大宮交番", "京都市上京区今出川通大宮上る観世町１４２", "075-451-0162", 35.029869, 135.748897),
            new PoliceContent(1, "大宮頭交番", "京都市上京区大宮通寺之内上る二丁目仲之町５００－２", "075-441-8467", 35.035611, 135.748776),
            new PoliceContent(1, "上御霊前交番", "京都市上京区烏丸通上御霊前下る相国寺門前町６４７－２０", "075-432-1658", 35.033835, 135.759568),
            new PoliceContent(1, "下立売交番", "京都市上京区下立売通智恵光院角中村町５３１", "075-841-6610", 35.019969, 135.746907),
            new PoliceContent(1, "千本中立売交番", "京都市上京区中立売通千本西入亀屋町６９３－３", "075-462-0155", 35.025127, 135.741836),
            new PoliceContent(1, "出町交番", "京都市上京区河原町通今出川上る青竜町２００", "075-231-3215", 35.031475, 135.769769),
            new PoliceContent(1, "室町中立売交番", "京都市上京区中立売通室町東入東町４７４", "075-441-2430", 35.025113, 135.758288),

            new PoliceContent(2, "東山警察署", "京都市東山区清水４丁目１８５－６", "075-525-0110", 34.996093, 135.777348),
            new PoliceContent(2, "三条大橋東交番", "京都市東山区大和大路通三条下る東入若松町４１２－３", "075-771-2300", 35.008064, 135.77351),
            new PoliceContent(2, "大和大路交番", "京都市東山区祇園町北側２３９", "075-561-0153", 35.003953, 135.773176),
            new PoliceContent(2, "祗園交番", "京都市東山区祇園町南側５５１", "075-561-3255", 35.003618, 135.776901),
            new PoliceContent(2, "松原交番", "京都市東山区松原通大和大路東弓矢町５２番地２", "075-561-0192", 35.152844, 136.899076),
            new PoliceContent(2, "大仏前交番", "京都市東山区正面通本町東入茶屋町５２７－２", "075-561-2544", 34.989399, 135.771408),
            new PoliceContent(2, "渋谷交番", "京都市東山区渋谷通東大路東入３丁目上馬町５３６－３", "075-561-0890", 34.991526, 135.77851),
            new PoliceContent(2, "泉涌寺交番", "京都市東山区泉涌寺門前町２２－５", "075-561-0963", 34.982415, 135.774195),
            new PoliceContent(2, "東福寺交番", "京都市東山区本町１５丁目７８１", "075-561-0408", 34.979818, 135.770602)
    };
}
