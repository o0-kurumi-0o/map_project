package com.example.kurumi.mapproject.Prefarance.MailCheck;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarActivity;

import com.example.kurumi.mapproject.R;

public class MailCheckActivity extends ActionBarActivity {


    private MailCheckFragment mailCheckFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail_check);

        if (savedInstanceState == null) {
            mailCheckFragment = new MailCheckFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.mainFrame, mailCheckFragment).commit();
        }
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mailCheckFragment != null) {
            getSupportFragmentManager().putFragment(outState, "mailCheckFragment", mailCheckFragment);
        }
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (mailCheckFragment == null) {
            mailCheckFragment = (MailCheckFragment) getSupportFragmentManager().getFragment(savedInstanceState, "mailCheckFragment");
        }
    }
}
