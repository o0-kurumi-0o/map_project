package com.example.kurumi.mapproject.Map;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kurumi.mapproject.AsyncTask.MarkerAsyncTaskLoader;
import com.example.kurumi.mapproject.Dialog.ErrorShowDialogFragment;
import com.example.kurumi.mapproject.Dialog.GpsErrorDialog;
import com.example.kurumi.mapproject.Dialog.MapSettingChangeDialogFragment;
import com.example.kurumi.mapproject.Dialog.ProgressDialogFragment;
import com.example.kurumi.mapproject.PubSub.BusHolder;
import com.example.kurumi.mapproject.PubSub.MapTitleChange;
import com.example.kurumi.mapproject.R;
import com.example.kurumi.mapproject.database.ContentDAO;
import com.example.kurumi.mapproject.database.ContentData;
import com.example.kurumi.mapproject.database.MySQLiteOpenHelper;
import com.example.kurumi.mapproject.database.PositionData;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, LoaderManager.LoaderCallbacks<ArrayList<PositionData>> {
    private GoogleMap mMap; //,.
    private HashMap<Marker, PositionData> eventMarkerMap = new HashMap<>();
    private View view;
    private TextView textViewTime;
    private TextView textViewPlace;
    private TextView textViewContent;
    ArrayList<ContentData> contentDatas = new ArrayList<>();
    private ContentDAO databaseDao;
    private Circle circle;

    private MapDialogState mapDialogState;

    private String[] lat_long = {"", ""};
    private Marker mark;
    private FrameLayout frame;


    //位置関係
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private Location mLastLocation;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = false;

    private LocationRequest mLocationRequest;

    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 10000; // 10 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 10; // 10 meters

    //1=一週間,2=一ヶ月,3=一年,4=すべての期間
    int default_select_period = 0;
    private ProgressDialogFragment progressDialog;

    public MapFragment() {
        // Required empty public constructor
    }


    //現在位置関係
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // First we need to check availability of play services
        if (checkPlayServices()) {
            // Building the GoogleApi client
            buildGoogleApiClient();

            createLocationRequest();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        view = rootView.findViewById(R.id.alertLayout);
        view.setVisibility(View.INVISIBLE);
        textViewPlace = (TextView) rootView.findViewById(R.id.textView);
        textViewTime = (TextView) rootView.findViewById(R.id.textView3);
        textViewContent = (TextView) rootView.findViewById(R.id.textView2);
        frame = (FrameLayout) rootView.findViewById(R.id.frame);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            getDatabaseContent();
            SharedPreferences spf = PreferenceManager.getDefaultSharedPreferences(getActivity());
            Boolean[] booleans = {spf.getBoolean("checkbox1", true), spf.getBoolean("checkbox2", true), spf.getBoolean("checkbox3", true), spf.getBoolean("checkbox4", true)};

            contentDatas = databaseDao.getMolester(booleans, default_select_period);

            //       contentDatas = databaseDao.getAll();
            mapDialogState = new MapDialogState(default_select_period, spf.getBoolean("checkbox1", true), spf.getBoolean("checkbox2", true), spf.getBoolean("checkbox3", true), spf.getBoolean("checkbox4", true));

        } else {
            contentDatas = (ArrayList<ContentData>) savedInstanceState.getSerializable("hashMap");
            mapDialogState = (MapDialogState) savedInstanceState.getSerializable("state");
        }
        setUpMapIfNeeded();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        checkPlayServices();

        // Resuming the periodic location updates
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("hashMap", contentDatas);
        outState.putSerializable("state", mapDialogState);
    }

    private void getDatabaseContent() {
        MySQLiteOpenHelper dbHelper = new MySQLiteOpenHelper(getActivity());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        databaseDao = new ContentDAO(db, dbHelper);

    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
                //       setUpButton();
                //     task = new Async(getActivity(), mMap, contentDatas, eventMarkerMap);
                //     task.execute();

                ConnectivityManager connectivity = (ConnectivityManager) getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo network = connectivity.getActiveNetworkInfo();
                if (network == null) {
                    ErrorShowDialogFragment errorShowDialogFragment = new ErrorShowDialogFragment();
                    Bundle message = new Bundle();
                    message.putString("title", "エラー");
                    message.putString("message", "ネットワークに接続されていません");
                    errorShowDialogFragment.setArguments(message);
                    errorShowDialogFragment.show(getChildFragmentManager(), "error");

                } else {
                    if (network.isConnected()) {

                        Bundle args = new Bundle();
                        args.putSerializable("key", contentDatas);
                        Loader loader = getLoaderManager().restartLoader(0, args, MapFragment.this);
                        loader.forceLoad();

                    } else {
                        ErrorShowDialogFragment errorShowDialogFragment = new ErrorShowDialogFragment();
                        Bundle message = new Bundle();
                        message.putString("title", "エラー");
                        message.putString("message", "ネットワークに接続されていません");
                        errorShowDialogFragment.setArguments(message);
                        errorShowDialogFragment.show(getChildFragmentManager(), "error");
                    }
                }
            }
        }
    }
/*
    private void setUpButton() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayLocation();
                view.setVisibility(View.INVISIBLE);
             //   final CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(new LatLng(my_position[0], my_position[1]), 15);
             //   mMap.moveCamera(cu);
            }
        });
    }
*/

    private void displayLocation() {

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            double latitude = mLastLocation.getLatitude();
            double longitude = mLastLocation.getLongitude();
            //        final CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15);
            //           mMap.moveCamera(cu);
            CameraPosition camerapos = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(15).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camerapos));
            //       Toast.makeText(getActivity(),"aOK",Toast.LENGTH_SHORT).show();
        } else {
            //GPSをONにしてもらう
            GpsErrorDialog gpsErrorDialog = new GpsErrorDialog();
            gpsErrorDialog.show(getChildFragmentManager(), "gps_no");
        }
    }


    private void setUpMap() {
        // latitude and longitude

        final CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(new LatLng(34.985442, 135.758466), 15);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng arg0) {
                view.setVisibility(View.INVISIBLE);
                if (circle != null) circle.remove();
            }
        });

// adding marker
        mMap.moveCamera(cu);
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (circle != null) {
                    circle.remove();
                }
                PositionData eventInfo = eventMarkerMap.get(marker);

                //事件なのか交番なのかの判定
                if (eventInfo.isIncident_or_policeBox()) {
                    CircleOptions circleOptions = new CircleOptions()
                            .center(new LatLng(marker.getPosition().latitude, marker.getPosition().longitude))
                            .radius(100).fillColor(getResources().getColor(R.color.map_circle)).strokeColor(getResources().getColor(R.color.map_circle));
                    circle = mMap.addCircle(circleOptions);
                    lat_long = new String[]{String.valueOf(eventInfo.getLat()), String.valueOf(eventInfo.getLng())};

                    textViewPlace.setText(eventInfo.getPlace());
                    textViewTime.setText(eventInfo.getDate());
                    textViewContent.setText(eventInfo.getContent());
                    view.setVisibility(View.VISIBLE);
                    LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View buttonView = layoutInflater.inflate(R.layout.map_alert_incident, null, false);
                    Button bu = (Button) buttonView.findViewById(R.id.button11);
                    bu.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setUpButtonNearest();
                        }
                    });
                    frame.removeAllViews();
                    frame.addView(buttonView);

                } else {
                    dialogChange_policeBox(eventInfo);

                }
                return false;
            }
        });

    }

    private void dialogChange_policeBox(final PositionData eventInfo) {
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View buttonView = layoutInflater.inflate(R.layout.map_alert_police_box, null, false);
        Button phoneButton = (Button) buttonView.findViewById(R.id.button9);
        phoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpButtonPhone();
            }
        });
        Button routeButton = (Button) buttonView.findViewById(R.id.button10);
        routeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpButtonRoute(eventInfo);
            }
        });

        frame.removeAllViews();
        frame.addView(buttonView);

        textViewPlace.setText(eventInfo.getTitle());
        textViewTime.setText(eventInfo.getPlace());
        textViewContent.setText(eventInfo.getContent());
        view.setVisibility(View.VISIBLE);
    }

    //ルート検索
    private void setUpButtonRoute(PositionData eventInfo) {
        //    displayLocation();
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            double latitude = mLastLocation.getLatitude();
            double longitude = mLastLocation.getLongitude();

            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            intent.setData(Uri.parse("http://maps.google.com/maps?saddr=" + latitude + "," + longitude + "&daddr=" + lat_long[0] + "," + lat_long[1]));
            startActivity(intent);

        } else {
            //GPSをONにしてもらう
            GpsErrorDialog gpsErrorDialog = new GpsErrorDialog();
            gpsErrorDialog.show(getChildFragmentManager(), "gps_no");
        }
    }


    //電話画面への遷移
    private void setUpButtonPhone() {

        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:0123456789"));
        PackageManager packageManager = getActivity().getPackageManager();
        List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);
        boolean isIntentSafe = activities.size() > 0;
        if (isIntentSafe) startActivity(intent);
        else Toast.makeText(getActivity(), "電話機能がありません", Toast.LENGTH_SHORT).show();
    }

    private void setUpButtonNearest() {
        if (mark != null) {
            mark.remove();
            eventMarkerMap.remove(mark);
        }
        getDatabaseContent();
        PositionData positionData = databaseDao.getNearestPoliceBox(lat_long);
        Marker m = mMap.addMarker(new MarkerOptions().position(new LatLng(positionData.getLat(),
                positionData.getLng())).title(positionData.getTitle()).
                icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
        eventMarkerMap.put(m, positionData);
        mark = m;

        CameraPosition camerapos = new CameraPosition.Builder().target(new LatLng(positionData.getLat(), positionData.getLng())).zoom(15).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camerapos));

        //     mMap.moveCamera(cu);

        textViewPlace.setText(positionData.getTitle());
        textViewTime.setText(positionData.getPlace());
        textViewContent.setText(positionData.getContent());
        view.setVisibility(View.VISIBLE);
        dialogChange_policeBox(positionData);
    }

    public void changePin(int selectedItemPosition, Boolean checkBox1, Boolean checkBox2, Boolean checkBox3, Boolean checkBox4) {
        //タイトルを変更
        String[] title = new String[]{"今週の犯罪", "今月の犯罪", "今年の犯罪", "すべての期間の犯罪"};
        BusHolder.get().post(new MapTitleChange(title[selectedItemPosition]));

        mapDialogState.setCheck1(checkBox1);
        mapDialogState.setCheck2(checkBox2);
        mapDialogState.setCheck3(checkBox3);
        mapDialogState.setCheck4(checkBox4);
        mapDialogState.setPeriod(selectedItemPosition);
        view.setVisibility(View.INVISIBLE);
        Boolean[] booleans = {checkBox1, checkBox2, checkBox3, checkBox4};
        //idがかぶるのはどうしよう
        mMap.clear();
        contentDatas.clear();

        getDatabaseContent();
        contentDatas = databaseDao.getMolester(booleans, selectedItemPosition);

        ConnectivityManager connectivity = (ConnectivityManager) getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
//ネットワークを確認して再度Async
        NetworkInfo network = connectivity.getActiveNetworkInfo();
        if (network == null) {
            ErrorShowDialogFragment errorShowDialogFragment = new ErrorShowDialogFragment();
            Bundle message = new Bundle();
            message.putString("title", "エラー");
            message.putString("message", "ネットワークに接続されていません");
            errorShowDialogFragment.setArguments(message);
            errorShowDialogFragment.show(getChildFragmentManager(), "error");

        } else {
            if (network.isConnected()) {
                Bundle args = new Bundle();
                args.putSerializable("key", contentDatas);
                Loader loader = getLoaderManager().restartLoader(0, args, MapFragment.this);
                loader.forceLoad();

            } else {
                ErrorShowDialogFragment errorShowDialogFragment = new ErrorShowDialogFragment();
                Bundle message = new Bundle();
                message.putString("title", "エラー");
                message.putString("message", "ネットワークに接続されていません");
                errorShowDialogFragment.setArguments(message);
                errorShowDialogFragment.show(getChildFragmentManager(), "error");
            }
        }


    }

    public void showSettingDialog() {
        MapSettingChangeDialogFragment settingChangeDialogFragment = new MapSettingChangeDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("state", mapDialogState);
        settingChangeDialogFragment.setArguments(bundle);
        settingChangeDialogFragment.show(getFragmentManager(), "tag");
    }


    //以下現在地

    /**
     * Method to verify google play services on the device
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(),
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getActivity(), "This device is not supported.", Toast.LENGTH_LONG).show();
                getActivity().finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * Creating location request object
     */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }


    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
        //勝手につけたし
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        //   displayLocation();
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        // Assign the new location
        mLastLocation = location;
        Toast.makeText(getActivity(), "Location changed!", Toast.LENGTH_SHORT).show();
        // Displaying the new location on UI
        displayLocation();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i("ee", "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    @Override
    public Loader<ArrayList<PositionData>> onCreateLoader(int id, Bundle args) {
        progressDialog = new ProgressDialogFragment();
        Bundle message = new Bundle();
        message.putString("tag", "情報読み込み中");
        progressDialog.setArguments(message);
        progressDialog.show(getChildFragmentManager(), "error");

        contentDatas = (ArrayList<ContentData>) args.getSerializable("key");

        return new MarkerAsyncTaskLoader(getActivity(), contentDatas);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<PositionData>> loader, ArrayList<PositionData> positionDatas) {
        if (progressDialog != null) {
            progressDialog.onDismiss(progressDialog.getDialog());
        }
        for (int i = 0; i < positionDatas.size(); i++) {
            Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(positionDatas.get(i).getLat(),
                    positionDatas.get(i).getLng())).title(positionDatas.get(i).getTitle()).
                    icon(BitmapDescriptorFactory.defaultMarker(positionDatas.get(i).getColor())));

            CircleOptions circleOptions = new CircleOptions()
                    .center(new LatLng(marker.getPosition().latitude, marker.getPosition().longitude))
                    .radius(100).fillColor(getActivity().getResources().getColor(R.color.map_circle)).strokeColor(getActivity().getResources().getColor(R.color.map_circle));
            mMap.addCircle(circleOptions);

            eventMarkerMap.put(marker, positionDatas.get(i));
        }

        Toast.makeText(getActivity(), "読み込み完了", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onLoaderReset(Loader<ArrayList<PositionData>> loader) {

    }

    public void my_location() {
        displayLocation();
        view.setVisibility(View.INVISIBLE);
    }

    /**
     * 最寄の交番を検索
     */
    public void nearest_police_box() {

        double latitude = 0;
        double longitude = 0;


        boolean find=false;
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();
            find=true;
        } else {
            //GPSをONにしてもらう
            GpsErrorDialog gpsErrorDialog = new GpsErrorDialog();
            gpsErrorDialog.show(getChildFragmentManager(), "gps_no");
        }
        if(find) {
            String[] myLocation = new String[]{String.valueOf(latitude), String.valueOf(longitude)};

            view.setVisibility(View.INVISIBLE);
            if (mark != null) {
                mark.remove();
                eventMarkerMap.remove(mark);
            }
            getDatabaseContent();
            PositionData positionData = databaseDao.getNearestPoliceBox(myLocation);
            Marker m = mMap.addMarker(new MarkerOptions().position(new LatLng(positionData.getLat(),
                    positionData.getLng())).title(positionData.getTitle()).
                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
            eventMarkerMap.put(m, positionData);
            mark = m;

            CameraPosition camerapos = new CameraPosition.Builder().target(new LatLng(positionData.getLat(), positionData.getLng())).zoom(15).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camerapos));

            textViewPlace.setText(positionData.getTitle());
            textViewTime.setText(positionData.getPlace());
            textViewContent.setText(positionData.getContent());
            view.setVisibility(View.VISIBLE);
            dialogChange_policeBox(positionData,myLocation);

        }
    }

    private void dialogChange_policeBox(final PositionData positionData, final String[] myLocation) {
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View buttonView = layoutInflater.inflate(R.layout.map_alert_police_box, null, false);
        Button phoneButton = (Button) buttonView.findViewById(R.id.button9);
        phoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpButtonPhone();
            }
        });
        Button routeButton = (Button) buttonView.findViewById(R.id.button10);
        routeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpButtonRoute(positionData.getLat(),positionData.getLng());
            }
        });

        frame.removeAllViews();
        frame.addView(buttonView);

        textViewPlace.setText(positionData.getTitle());
        textViewTime.setText(positionData.getPlace());
        textViewContent.setText(positionData.getContent());
        view.setVisibility(View.VISIBLE);
    }

    private void setUpButtonRoute(double s, double s1) {
        //    displayLocation();
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            double latitude = mLastLocation.getLatitude();
            double longitude = mLastLocation.getLongitude();

            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            intent.setData(Uri.parse("http://maps.google.com/maps?saddr=" + latitude + "," + longitude + "&daddr=" + s + "," + s1));
            startActivity(intent);

        } else {
            //GPSをONにしてもらう
            GpsErrorDialog gpsErrorDialog = new GpsErrorDialog();
            gpsErrorDialog.show(getChildFragmentManager(), "gps_no");
        }
    }


}
