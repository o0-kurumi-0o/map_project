package com.example.kurumi.mapproject.PoliceBox;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.kurumi.mapproject.PoliceBox.MAP.PoliceBoxMapActivity;
import com.example.kurumi.mapproject.R;
import com.example.kurumi.mapproject.database.ContentDAO;
import com.example.kurumi.mapproject.database.MySQLiteOpenHelper;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class PoliceBoxActivityFragment extends Fragment {

    private ListView listView;
    private ArrayList<PoliceContent> arrayList;
    private ContentDAO databaseDao;

    public PoliceBoxActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_police_box, container, false);
        listView=(ListView)rootView.findViewById(R.id.listView2);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        int position=getArguments().getInt("position");
        MySQLiteOpenHelper dbHelper = new MySQLiteOpenHelper(getActivity());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        databaseDao = new ContentDAO(db, dbHelper);
        arrayList=databaseDao.getPoliceBox(position);

        policeBoxAdapter policeBoxAdapter=new policeBoxAdapter(getActivity(),arrayList);
        listView.setAdapter(policeBoxAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(getActivity(), PoliceBoxMapActivity.class);
                intent.putExtra("content",arrayList);
                intent.putExtra("position",position);
                startActivity(intent);
            }
        });
    }

    public static PoliceBoxActivityFragment newInstance(int position) {
        PoliceBoxActivityFragment fragment = new PoliceBoxActivityFragment();
        Bundle args = new Bundle();
        args.putInt("position",position);
        fragment.setArguments(args);
        return fragment;
    }
}
