package com.example.kurumi.mapproject.PoliceBox.MAP;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kurumi.mapproject.PoliceBox.PoliceContent;
import com.example.kurumi.mapproject.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A placeholder fragment containing a simple view.
 */
public class PoliceBoxMapActivityFragment extends Fragment {
    private GoogleMap mMap; //,.
    private View view;
    private TextView textViewTime;
    private TextView textViewPlace;
    private TextView textViewContent;
    private ArrayList<PoliceContent> policeContents = new ArrayList<>();
    private HashMap<Marker,PoliceContent> eventMarkerMap= new HashMap<>();
    private int position;


    public PoliceBoxMapActivityFragment() {
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            policeContents =(ArrayList<PoliceContent>) getArguments().getSerializable("content");
            position=getArguments().getInt("position");
        }else {
            policeContents = (ArrayList<PoliceContent>) savedInstanceState.getSerializable("content");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("content", policeContents);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        view = rootView.findViewById(R.id.alertLayout);
        view.setVisibility(View.INVISIBLE);
        textViewPlace = (TextView) rootView.findViewById(R.id.textView);
        textViewTime = (TextView) rootView.findViewById(R.id.textView3);
        textViewContent = (TextView) rootView.findViewById(R.id.textView2);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
            if (mMap != null) {
                setUpMap();
                Async3 task = new Async3(getActivity(), mMap, policeContents,eventMarkerMap);
                task.execute();

            }
        }
    }

    private void setUpMap() {
        // latitude and longitude


        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng arg0) {
                view.setVisibility(View.INVISIBLE);
            }
        });

// adding marker
        CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(new LatLng(policeContents.get(position).getLatitude(), policeContents.get(position).getLongitude()), 15);
        mMap.moveCamera(cu);
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                PoliceContent eventInfo = eventMarkerMap.get(marker);
                textViewPlace.setText(eventInfo.getPoliceBoxName());
                textViewTime.setText(eventInfo.getPoliceBoxPlace());
                textViewContent.setText(eventInfo.getPoliceBoxPhoneNumber());
                view.setVisibility(View.VISIBLE);
                return false;
            }
        });


    }
}
