package com.example.kurumi.mapproject.News.MAP;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.kurumi.mapproject.R;
import com.example.kurumi.mapproject.database.ContentData;

public class MapActivity2 extends AppCompatActivity {

    private MapFragment2 mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        Intent intent=getIntent();
        ContentData contentData = (ContentData) intent.getSerializableExtra("content");

        if(savedInstanceState==null){
            mapFragment=new MapFragment2();
            Bundle args_t = new Bundle();
            args_t.putSerializable("content", contentData);
            mapFragment.setArguments(args_t);
            getSupportFragmentManager().beginTransaction().add(R.id.mainFrame,mapFragment).commit();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
