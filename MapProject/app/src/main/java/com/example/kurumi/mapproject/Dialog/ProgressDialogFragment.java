package com.example.kurumi.mapproject.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.kurumi.mapproject.R;


/**
 * Created by kurumi on 2015/07/11.
 */
public class ProgressDialogFragment extends DialogFragment {


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        //    View rootView = inflater.inflate(R.layout.dialog_process, null, false);
        View rootView = inflater.inflate(R.layout.dialog_progress, null, false);
        TextView textView=(TextView)rootView.findViewById(R.id.textView12);
        String text=getArguments().getString("tag","");
        textView.setText(text);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setView(rootView);
        return  builder.create();
    }
}
