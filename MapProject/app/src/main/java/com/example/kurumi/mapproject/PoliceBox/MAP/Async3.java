package com.example.kurumi.mapproject.PoliceBox.MAP;

import android.content.Context;
import android.os.AsyncTask;

import com.example.kurumi.mapproject.PoliceBox.PoliceContent;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by kurumi on 2015/09/07.
 */
public class Async3 extends AsyncTask<Void, Void, Void> {
    private HashMap<Marker, PoliceContent> eventMarkerMap;
    private Context mContext;
    private GoogleMap map;
    private ArrayList<PoliceContent> policeContents;

    private Float[] markerColor = {BitmapDescriptorFactory.HUE_YELLOW, BitmapDescriptorFactory.HUE_GREEN, BitmapDescriptorFactory.HUE_RED};

    public Async3(Context mContext, GoogleMap mMap, ArrayList<PoliceContent> policeContents, HashMap<Marker, PoliceContent> eventMarkerMap) {
        this.mContext = mContext;
        this.map = mMap;
        this.policeContents = policeContents;
        this.eventMarkerMap = eventMarkerMap;
    }
    protected Void doInBackground(Void... params) {
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        for (int i = 0; i < policeContents.size(); i++) {
            Marker m = map.addMarker(new MarkerOptions().position(new LatLng(policeContents.get(i).getLatitude(), policeContents.get(i).getLongitude()))
                    .title(policeContents.get(i).getPoliceBoxName()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
            eventMarkerMap.put(m, policeContents.get(i));
        }
    }
}