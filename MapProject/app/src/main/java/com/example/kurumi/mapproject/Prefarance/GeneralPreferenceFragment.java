package com.example.kurumi.mapproject.Prefarance;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;

import com.example.kurumi.mapproject.AsyncTask.AsyncHttpRequest;
import com.example.kurumi.mapproject.Dialog.P_ErrorShowDialogFragment;
import com.example.kurumi.mapproject.Prefarance.MailCheck.MailCheckActivity;
import com.example.kurumi.mapproject.PubSub.BusHolder;
import com.example.kurumi.mapproject.PubSub.PreferenceClick;
import com.example.kurumi.mapproject.R;


/**
 * Created by kurumi on 2015/07/28.
 */
public class GeneralPreferenceFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.pref_general);

        PreferenceScreen screen=(PreferenceScreen) findPreference("getMailSelect");
        screen.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                BusHolder.get().post(new PreferenceClick());
                return false;
            }
        });



        Preference preference_mail = findPreference("mailCheck");
        preference_mail.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                // Intentインスタンスを生成
                Intent intent = new Intent(getActivity(), MailCheckActivity.class);
                startActivity(intent);
                return true;
            }
        });

        Preference preference_mail_get = findPreference("mailGet");
        preference_mail_get.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
                String a = sharedPref.getString("user", "");
                String b = sharedPref.getString("password", "");
                String c = sharedPref.getString("port", "");
                String d = sharedPref.getString("host", "");
                if (a.length() != 0 && b.length() != 0 && c.length() != 0 && d.length() != 0) {
                    AsyncHttpRequest task = new AsyncHttpRequest(getActivity());
                    task.execute();
                } else {
                    P_ErrorShowDialogFragment errorShowDialogFragment = new P_ErrorShowDialogFragment();
                    Bundle message = new Bundle();
                    message.putString("title", getResources().getString(R.string.error_title));
                    message.putString("message", "メールを設定してください");
                    errorShowDialogFragment.setArguments(message);
                    errorShowDialogFragment.show(getFragmentManager(), "error");
                }
                return  true;
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    }
}
