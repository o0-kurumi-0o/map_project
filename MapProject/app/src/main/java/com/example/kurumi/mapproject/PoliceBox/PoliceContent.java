package com.example.kurumi.mapproject.PoliceBox;

import java.io.Serializable;

/**
 * Created by kurumi on 2015/09/07.
 */
public class PoliceContent implements Serializable {
    String PoliceBoxName;
    int PoliceBoxTag;
    String PoliceBoxPlace;
    String PoliceBoxPhoneNumber;
    double latitude;
    double longitude;


    public PoliceContent(int tag,String policeBoxName,String place , String policeBoxPhoneNumber, double latitude, double longitude) {
        this.PoliceBoxTag = tag;
        this.PoliceBoxPlace=place;
        this.PoliceBoxName = policeBoxName;
        this.PoliceBoxPhoneNumber = policeBoxPhoneNumber;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public PoliceContent() {
    }

    public String getPoliceBoxName() {
        return PoliceBoxName;
    }

    public void setPoliceBoxName(String policeBoxName) {
        PoliceBoxName = policeBoxName;
    }

    public String getPoliceBoxPhoneNumber() {
        return PoliceBoxPhoneNumber;
    }

    public void setPoliceBoxPhoneNumber(String policeBoxPhoneNumber) {
        PoliceBoxPhoneNumber = policeBoxPhoneNumber;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getPoliceBoxTag() {
        return PoliceBoxTag;
    }

    public void setPoliceBoxTag(int policeBoxTag) {
        PoliceBoxTag = policeBoxTag;
    }

    public String getPoliceBoxPlace() {
        return PoliceBoxPlace;
    }

    public void setPoliceBoxPlace(String policeBoxPlace) {
        PoliceBoxPlace = policeBoxPlace;
    }
}
