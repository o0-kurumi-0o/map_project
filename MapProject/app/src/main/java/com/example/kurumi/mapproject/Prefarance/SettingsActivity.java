package com.example.kurumi.mapproject.Prefarance;


import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.kurumi.mapproject.PubSub.BusHolder;
import com.example.kurumi.mapproject.PubSub.PreferenceClick;
import com.example.kurumi.mapproject.R;
import com.squareup.otto.Subscribe;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p/>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends ActionBarActivity {

    private static final String TAG_NESTED = "TAG_NESTED";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        setupToolBar();
   //     setupToolBar();
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().replace(R.id.settinghMain, new GeneralPreferenceFragment()).commit();
        }
    }

    private void setupToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("設定");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_up);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Subscriberとして登録する
        BusHolder.get().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Subscriberの登録を解除する
        BusHolder.get().unregister(this);
    }


    @Override
    public void onBackPressed() {
        // this if statement is necessary to navigate through nested and main fragments
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();
        }
    }

    @Subscribe
    public void subscribe(PreferenceClick event) {
        getFragmentManager().beginTransaction().replace(R.id.settinghMain,
                NestedPreferenceFragment.newInstance(event.key), TAG_NESTED).addToBackStack(TAG_NESTED).commit();

    }

}
