package com.example.kurumi.mapproject.PoliceBox.MAP;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.kurumi.mapproject.PoliceBox.PoliceContent;
import com.example.kurumi.mapproject.R;

import java.util.ArrayList;

public class PoliceBoxMapActivity extends AppCompatActivity {
    private Toolbar toolbar;
    PoliceBoxMapActivityFragment policeBoxMapActivityFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        setupToolBar();

        Intent intent=getIntent();
        ArrayList<PoliceContent> contentData = (ArrayList<PoliceContent>) intent.getSerializableExtra("content");
        int position = intent.getIntExtra("position",0);

        if (savedInstanceState == null) {
            policeBoxMapActivityFragment = new PoliceBoxMapActivityFragment();
            Bundle args_t = new Bundle();
            args_t.putSerializable("content", contentData);
            args_t.putInt("position", position);
            policeBoxMapActivityFragment.setArguments(args_t);
            getSupportFragmentManager().beginTransaction().add(R.id.mainFrame, policeBoxMapActivityFragment).commit();
        }
    }


    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (policeBoxMapActivityFragment != null) {
            getSupportFragmentManager().putFragment(outState, "mapFragment", policeBoxMapActivityFragment);
        }
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (policeBoxMapActivityFragment == null) {
            policeBoxMapActivityFragment = (PoliceBoxMapActivityFragment) getSupportFragmentManager().getFragment(savedInstanceState, "mapFragment");
        }
    }



    private void setupToolBar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("交番情報");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_up);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_police_box_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
