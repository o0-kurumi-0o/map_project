package com.example.kurumi.mapproject.News;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.kurumi.mapproject.News.MAP.MapActivity2;
import com.example.kurumi.mapproject.R;
import com.example.kurumi.mapproject.database.ContentDAO;
import com.example.kurumi.mapproject.database.ContentData;
import com.example.kurumi.mapproject.database.MySQLiteOpenHelper;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class NewsActivityFragment extends Fragment {

    private ListView listView;
    private ArrayList<ContentData> contentDatas=new ArrayList<>();

    public NewsActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState==null) {
            int position=getArguments().getInt("position");

            MySQLiteOpenHelper dbHelper = new MySQLiteOpenHelper(getActivity());
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentDAO databaseDao = new ContentDAO(db, dbHelper);
            if(position==0) {
                contentDatas = databaseDao.getAll();
            }else{
                contentDatas = databaseDao.selectNews(position-1);
            }

        }else {
            contentDatas= (ArrayList<ContentData>) savedInstanceState.getSerializable("EXTRA_LISTDATA");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_news, container, false);
        listView=(ListView)rootView.findViewById(R.id.listView);
        return rootView;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        NewsArrayAdapter adapter=new NewsArrayAdapter(getActivity(),1,contentDatas);
        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(getActivity(),MapActivity2.class);

                intent.putExtra("content",contentDatas.get(position));
                getActivity().startActivity(intent);
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("EXTRA_LISTDATA", contentDatas);
    }

    public static NewsActivityFragment newInstance(int position) {
        NewsActivityFragment fragment = new NewsActivityFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        fragment.setArguments(args);
        return fragment;
    }

}
