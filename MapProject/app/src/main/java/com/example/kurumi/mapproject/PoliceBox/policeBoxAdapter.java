package com.example.kurumi.mapproject.PoliceBox;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.kurumi.mapproject.R;

import java.util.ArrayList;

/**
 * Created by kurumi on 2015/09/07.
 */
public class policeBoxAdapter extends ArrayAdapter<PoliceContent> {

    private ArrayList<PoliceContent> policeContents;
    Context content;

    public policeBoxAdapter(Context context, ArrayList<PoliceContent> resource) {
        super(context, 0,resource);
        this.policeContents=resource;
    this.content=context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.police_list_inner, parent, false);
        }
        TextView tvName = (TextView) convertView.findViewById(R.id.textView11);
        TextView tvPosition = (TextView) convertView.findViewById(R.id.textView14);
        TextView tvPhone=(TextView)convertView.findViewById(R.id.textView13);

        tvName.setText(policeContents.get(position).getPoliceBoxName());
        tvPosition.setText(policeContents.get(position).getPoliceBoxPlace());
        tvPhone.setText(policeContents.get(position).getPoliceBoxPhoneNumber());

        return convertView;
    }
}
